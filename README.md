# Ardour cheat-sheet

## Description

[Ardour](https://ardour.org/) shortcuts cheat-sheet.  Short, Visual, printable (A4 and Letter).

I started multiple ones:
* There is a LaTex file in this repo.
* Draft on https://cheatography.com/brnvrn/cheat-sheets/ardour/ (too early to publish it, but I can add collaborators)
* Draft on https://www.OverLeaf.com/project/659424983865d0b8b41cca67
 

### Existing ones

* Official exhaustive list https://manual.ardour.org/appendix/default-keyboard-shortcuts/
* https://DefKey.com/ardour-shortcuts
* Other sites with tons of shortcuts (but no Ardour) https://KeyCheck.dev/apps/ ; https://KeyCombiner.com/


### Inspiration, examples

* https://blog.devops.dev/mastering-vim-essential-keyboard-shortcuts-for-productivity-and-efficiency-7a759ae1a3ae
* https://bjosephburch.com/ultimate-roam-shortcuts-cheat-sheet/
* https://www.templateroller.com/template/2636709/vim-graphical-cheat-sheet.html#doc_window
* https://raw.githubusercontent.com/nachazo/blender-cheat-sheet/main/blender-cheat-sheet.png
* https://www.amazon.co.uk/Keyboard-Shortcuts-Shortcut-Language-Computer/dp/B0CNTH99KJ


## Visual (draft)

![Early draft](draft.png)


## Installation
 
Online tools do not require anything, but local LaTex development will require:
`sudo dnf install texlive-latex texlive-overpic texlive-mathdesign texlive-tcolorbox texlive-tikzfill texlive-pdfcol texlive-listingsutf8 texlive-multirow texlive-collection-fontsrecommended texlive-menukeys` and maybe more :-)

These are useful for development (Formatter and Linter):
`sudo dnf install texlive-latexindent texlive-chktex`


## Usage

I do not know about LaTeX command line.  I used [TeXstudio](https://flathub.org/apps/org.texstudio.TeXstudio).


## TODO 

* Break it down by usage or window:
  * Editor
  * Mixer
  * MIDI editing
* Make it A4 instead of Letter


## Dev

There are a few helpful templates: 
* https://www.OverLeaf.com/latex/templates/colourful-cheatsheet-template/qdsshbjktndd 
* https://www.overleaf.com/latex/templates/overleaf-keyboard-shortcuts/pphdnzrwmttk#
* https://tex.stackexchange.com/questions/590198/multi-colored-cheat-sheet-for-keyboard-shortcuts
* https://tex.stackexchange.com/questions/5226/keyboard-font-for-latex
* https://tex.stackexchange.com/questions/706067/keyboards-key-visual-formatting-for-keyboard-shortcuts-kind-of-cheat-sheet



## Project status

Early draft.
